export default class TimelineApi {
  
  static lista(endpoint) {
    return dispatch => {
      fetch(endpoint)
        .then(res => res.json())
        .then(fotos => {
          dispatch({type: 'LISTAGEM', fotos})
          return fotos
        })
    }
  }

}