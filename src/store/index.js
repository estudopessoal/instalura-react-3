import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { timeline } from '../reducers/timeline'

export default createStore(timeline, applyMiddleware(thunkMiddleware))
