import React, { Component } from 'react'
import TimelineApi from '../../services/TimelineApi'
import Foto from '../Foto/Foto'
import './Timeline.css'

export default class Timeline extends Component {
  
  constructor() {
    super()
    this.state = {fotos: []}
  }

  componentWillMount() {
    this.props.store.subscribe(() => this.setState({fotos: this.props.store.getState()}))
  }
  
  componentDidMount() {
    this.props.store.dispatch(TimelineApi.lista('/api/timeline.json'))
  }

  render () {
    return (
      <div className="timeline container">
        {
          this.state.fotos.map((foto, index) => 
            <Foto
              key={index}
              user={foto.user}
              image={foto.image}
              liked={foto.liked}
              comments={foto.comments}
            />)
        }
      </div>
    )
  }
}