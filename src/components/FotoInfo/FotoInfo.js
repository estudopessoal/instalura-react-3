import React, { Component } from 'react';
import './FotoInfo.css'

export default class FotoInfo extends Component {
  render () {
    return (
      <div className="foto-info">
        <div className="foto-info-likes">
          { this.props.liked.map((like, index) => <a key={index}>{like.user}, </a>) } 
          { this.props.liked.length ? 'curtiram':'' }
        </div>

        <ul className="foto-info-comentarios">
          {
            this.props.comments.map((comment, index) => 
              <li className="comentario" key={index}>
                <a className="foto-info-autor">{comment.user} </a>
                {comment.text}
              </li>
            )
          }
        </ul>
      </div>

    );
  }
}